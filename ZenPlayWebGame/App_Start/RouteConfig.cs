﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ZenPlayWebGame
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
             "d",
              "d/{alias}",
              new { controller = "Home", action = "Detail", alias = UrlParameter.Optional }
          );

            routes.MapRoute(
         "download",
          "download/android",
          new { controller = "Home", action = "DownloadGameAndroid" }
      );
            routes.MapRoute(
     "download2",
      "download/ios",
      new { controller = "Home", action = "DownloadGameIOS" }
  );

            routes.MapRoute(
           "c",
            "c/{status}",
            new { controller = "Home", action = "News", status = UrlParameter.Optional }
        );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }               
            );

          
        }
    }
}
