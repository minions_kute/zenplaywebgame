﻿using System.Web;
using System.Web.Mvc;

namespace ZenPlayWebGame
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
