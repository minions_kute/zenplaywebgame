﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web;
using Newtonsoft.Json;

using System.Security.Cryptography;

    public class ServerProcess
    {
        private const string RequestTokenUrlFormat = "{0}oauth/authorize?client_key={1}&ur={2}&mode={3}&redirect_uri={4}&partner_id={5}&app_id={6}&ads_campaign_id={7}&ads_channel_id={8}";
        private const string AccessTokenUrlFormat = "{0}oauth/accesstoken?client_key={1}&request_token={2}&request_time={3}&sign={4}";
        private const string RefreshTokenUrlFormat = "{0}oauth/refreshtoken?client_key={1}&refresh_token={2}&request_time={3}&sign={4}";
        private const string RegisterUrlFormat = "{0}oauth/register?client_key={1}&ur={2}&mode={3}&redirect_uri={4}";
        private const string AddCoinUrlFormat = "{0}nap-vao-game/nap-xu-qua-the-cao/{1}/?ext_gc={2}";

        private const string LogoutUrlFormat = "{0}oauth/logout?access_token={1}&ur={2}";

        public static readonly string ServerApp = ConfigurationManager.AppSettings["server_app"];

        private static readonly string ServerIdGraph = ConfigurationManager.AppSettings["id_graph"];
        private static readonly string id_billing = ConfigurationManager.AppSettings["id_billing"];
     
        
        private static readonly string DomainGeneralGraph = ConfigurationManager.AppSettings["general_graph"];

        private static readonly string ServerProfile = ConfigurationManager.AppSettings["profile"];

        public static readonly string Mode = ConfigurationManager.AppSettings["mode"];
        public static readonly string ClientKey = ConfigurationManager.AppSettings["client_key"];
        public static readonly string PrivateKey = ConfigurationManager.AppSettings["PrivateKey"];
        public static readonly string ClientSecret = ConfigurationManager.AppSettings["client_secret"];
        public static readonly string Agency_id = ConfigurationManager.AppSettings["agency_id"];
        public static readonly string Game_Code = ConfigurationManager.AppSettings["game_code"];
        public static readonly string Game_server = ConfigurationManager.AppSettings["game_server"];

        private const string BalanceUrlFormat = "{0}billing/getbalance/?access_token={1}";

        private const string ProfileUrlFormat = "{0}account/GetProfile/?access_token={1}";

   


        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static string GetGamePayAddCoin()
        {
            return string.Format(AddCoinUrlFormat, id_billing, Game_Code, Game_Code);
        }

        public static string GetProfileUrl()
        {
            return ServerProfile;
        }
        /// <summary>
        /// Build url gọi Graph để authen
        /// </summary>
        /// <returns></returns>
        public static string GetRequestTokenUrl(int partner_id, int app_id, int ads_campaign_id, int ads_channel_id)
        {
            var ur = HttpContext.Current.Request.Params["ur"] ?? string.Empty;

            if (string.IsNullOrEmpty(ur) && HttpContext.Current.Request.UrlReferrer != null)
                ur = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;

            var redirectUri = ServerApp + "ConnectGraph/Authorization.aspx";
            // Build url gọi Graph để authen
            return string.Format(RequestTokenUrlFormat, ServerIdGraph, ClientKey
                , HttpUtility.UrlEncode(ur), Mode, HttpUtility.UrlEncode(redirectUri),partner_id,app_id,ads_campaign_id,ads_channel_id);
        }

        /// <summary>
        /// Build url gọi Graph để Logout
        /// </summary>
        /// <returns></returns>
        public static string GetRejectTokenUrl()
        {
            var ur = HttpContext.Current.Request.Params["ur"] ?? string.Empty;

            if (string.IsNullOrEmpty(ur) && HttpContext.Current.Request.UrlReferrer != null)
                ur = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;

            var redirectUri = ServerApp + "ConnectGraph/Authorization.aspx?ur={0}";
            redirectUri = string.Format(redirectUri, ur);
            // Build url gọi Graph để authen
            return string.Format(LogoutUrlFormat, ServerIdGraph, AccountSession.AccessToken, HttpUtility.UrlEncode(redirectUri));
        }

        /// <summary>
        /// Build url redirect sang trang success
        /// </summary>
        /// <returns>url và accountId</returns>
        public static string GetSuccessUrl()
        {
            return string.Format("{0}ConnectGraph/Success.aspx?account_name={1}", ServerApp, AccountSession.AccountName);
        }

        /// <summary>
        /// Lấy accesstoken, lưu vào session
        /// </summary>
        /// <returns></returns>
        public static string GetAccessToken(bool total = false)
        {
            // Lấy request_token 
            var requestToken = HttpContext.Current.Request.Params["request_token"];

            if (!string.IsNullOrEmpty(requestToken))
            {
                // Chữ ký của ServerApp gửi Graph
                var requestTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                var sign = string.Format("{0}|{1}|{2}|{3}", ClientKey, requestToken, requestTime, ClientSecret);

                // Build url để lấy access_token
                var getAccessToken = string.Format(AccessTokenUrlFormat, ServerIdGraph, ClientKey, requestToken, requestTime, Utilities.Md5(sign));

                var data = Utilities.HttpRequestGet(getAccessToken);
                // Get access_token
                var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(data);

                // code < 0 trong mọi trường hợp đều là lỗi.
                if (apiResponse.Code >= 0)
                {
                    var dic = JsonConvert.DeserializeObject<Dictionary<string, string>>(apiResponse.Data);

                    // Lưu account_id, account_name, access_token, refresh_token vào session.
                    AccountSession.AccountId = dic["account_id"];
                    AccountSession.AccountName = dic["account_name"];
                    AccountSession.RefreshToken = dic["refresh_token"];
                    AccountSession.AccessToken = dic["access_token"];
               
                    return AccountSession.AccountId;
                }
            }else
            {
                AccountSession.AccountId = string.Empty;
                AccountSession.AccountName =string.Empty;
                AccountSession.RefreshToken = string.Empty;
                AccountSession.AccessToken =string.Empty;
            }
            return string.Empty;
        }
         

        public static string GetProfile(bool allowRefreshToken = true)
        {
            var requestUrl = string.Format(ProfileUrlFormat, DomainGeneralGraph, AccountSession.AccessToken);
            var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(Utilities.HttpRequestGet(requestUrl));

            // Trường hợp lỗi access_token expires, thực hiện Renew access_token và gọi lại hàm với thàm số 
            //allowRefreshToken = false tránh tình trạng lặp
            if (apiResponse.Code == -103 && allowRefreshToken && RefreshAccessToken() >= 0)
            {
                return GetProfile(false);
            }

            return apiResponse.Code >= 0 ? apiResponse.Data : string.Empty;
        }


       
        /// <summary>
        /// Renew access_token
        /// </summary>
        /// <returns>>=0: Renew thành công; </returns>
        private static int RefreshAccessToken()
        {
            var requestTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            var sign = Utilities.Md5(string.Format("{0}|{1}|{2}|{3}"
                , ClientKey, AccountSession.RefreshToken, requestTime, ClientSecret));
            var requestUrl = string.Format(RefreshTokenUrlFormat
                , ServerIdGraph, ClientKey, AccountSession.RefreshToken, requestTime, sign);
            var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(Utilities.HttpRequestGet(requestUrl));
            if (apiResponse.Code >= 0)
            {
                var dic = JsonConvert.DeserializeObject<Dictionary<string, string>>(apiResponse.Data);
                AccountSession.AccessToken = dic["access_token"];
                AccountSession.RefreshToken = dic["refresh_token"];
            }
            return apiResponse.Code;
        }

        public static string GetRegisterUrl()
        {
            var ur = HttpContext.Current.Request.Params["ur"] ?? string.Empty;

            if (string.IsNullOrEmpty(ur) && HttpContext.Current.Request.UrlReferrer != null)
                ur = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;

            var redirectUri = string.Format("{0}connectgraph/Authorization.aspx"
                , ServerApp);
            // Build url gọi Graph để register
            return string.Format(RegisterUrlFormat, ServerIdGraph, ClientKey, HttpUtility.UrlEncode(ur), Mode, HttpUtility.UrlEncode(redirectUri));
        }



       
    }
