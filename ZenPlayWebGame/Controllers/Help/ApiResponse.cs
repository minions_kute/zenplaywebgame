﻿using System;
using Newtonsoft.Json;

    [Serializable]
    public class ApiResponse
    {
        [JsonProperty(PropertyName = "code")]
        public int Code { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "data")]
        public string Data { get; set; }

        [JsonProperty(PropertyName = "response_time")]
        public string ResponseTime { get; set; }

        [JsonProperty(PropertyName = "sign")]
        public string Sign { get; set; }
         [JsonProperty(PropertyName = "requestNo")]
        public string RequestNo { get; set; }
    }
