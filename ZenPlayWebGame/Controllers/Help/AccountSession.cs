﻿using System.Web;


    public class AccountSession
    {
        public static string AccountId
        {
            set
            {
                HttpContext.Current.Session["zenplay.com_account_id"] = value;
            }
            get
            {
                return HttpContext.Current.Session["zenplay.com_account_id"] as string;
            }
        }

        public static string AccountName
        {
            set
            {
                HttpContext.Current.Session["zenplay.com_account_name"] = value;
            }
            get
            {
                return HttpContext.Current.Session["zenplay.com_account_name"] as string;
            }
        }

        public static string AccessToken
        {
            set
            {
                HttpContext.Current.Session["zenplay.com_access_token"] = value;
            }
            get
            {
                return HttpContext.Current.Session["zenplay.com_access_token"] as string;
            }
        }

        public static string RefreshToken
        {
            set
            {
                HttpContext.Current.Session["zenplay.com_refresh_token"] = value;
            }
            get
            {
                return HttpContext.Current.Session["zenplay.com_refresh_token"] as string;
            }
        }
    }
