﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;

public class MyPolicy : ICertificatePolicy
{
    public bool CheckValidationResult(ServicePoint srvPoint,
      X509Certificate certificate, WebRequest request,
      int certificateProblem)
    {
        //Return True to force the certificate to be accepted.
        return true;
    }
}
    public static class Utilities
    {
        public static string ByteToHex(byte[] InBytes)
        {
            string str = "";
            foreach (byte num in InBytes)
            {
                str = str + string.Format("{0:x2}", num);
            }
            return str;
        }
        public static string Md5(string strSource)
        {
            byte[] pass = System.Text.Encoding.UTF8.GetBytes(strSource);
            MD5 md5 = new MD5CryptoServiceProvider();
            pass = md5.ComputeHash(pass);
            return ByteToHex(pass);
        }
        //public static string Md5(string input)
        //{
        //    try
        //    {
        //        //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
        //        MD5 md5 = new MD5CryptoServiceProvider();
        //        var originalBytes = Encoding.Default.GetBytes(input);
        //        var encodedBytes = md5.ComputeHash(originalBytes);

        //        //Convert encoded bytes back to a 'readable' string
        //        return BitConverter.ToString(encodedBytes).ToLower().Replace("-", "");
        //    }
        //    catch
        //    {
        //        return string.Empty;
        //    }
        //}

        public static string HttpRequestGet(string url)
        {
            var result = string.Empty;
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                //request.Proxy = null;
                request.UseDefaultCredentials = true;
                request.CookieContainer = new CookieContainer();
                request.Method = "GET";
                request.ContentType = "application/x-www-form-urlencoded";
                //request.ContentType = "text/xml; encoding='utf-8'";
                request.KeepAlive = false;
                request.AllowAutoRedirect = true;
                var response = (HttpWebResponse)request.GetResponse();
                var stream = response.GetResponseStream();
                if (stream != null)
                {
                    result = new StreamReader(stream).ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }

        public static string HttpRequestPost(string url, byte[] postData)
        {
            string result = string.Empty;
            try
            {
#pragma warning disable CS0618 // 'ServicePointManager.CertificatePolicy' is obsolete: 'CertificatePolicy is obsoleted for this type, please use ServerCertificateValidationCallback instead. http://go.microsoft.com/fwlink/?linkid=14202'
                System.Net.ServicePointManager.CertificatePolicy = new MyPolicy();
#pragma warning restore CS0618 // 'ServicePointManager.CertificatePolicy' is obsolete: 'CertificatePolicy is obsoleted for this type, please use ServerCertificateValidationCallback instead. http://go.microsoft.com/fwlink/?linkid=14202'
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.UseDefaultCredentials = true;
                request.CookieContainer = new CookieContainer();
                request.Method = "POST";
               
                request.ContentType = "application/x-www-form-urlencoded";
                //request.ContentType = "text/xml; encoding='utf-8'";
                request.ContentLength = postData.Length;
                //myRequest.KeepAlive = false;
                request.AllowAutoRedirect = true;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(postData, 0, postData.Length);
                    stream.Close();
                }
                var response = (HttpWebResponse)request.GetResponse();
                using (var stream = response.GetResponseStream())
                {
                    if (stream != null)
                    {
                        result = new StreamReader(stream).ReadToEnd();
                        stream.Close();
                        response.Close();
                    }
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                result = null;
            }
            return result;
        }
    }
