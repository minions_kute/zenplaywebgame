﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ZenPlayWebGame.Models
{
    public class WebNew
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ImageIntro { get; set; }

        public int HotNew { get; set; }//0, 1: small, 2: big
        public string Alias { get; set; }// tên thay thế
        public int Status { get; set; }// trạng thái

        public string ShortDes { get; set; }

        [AllowHtml]
        public string Content { get; set; }

        public int CategoryId { get; set; }

        public string Tags { get; set; }

        public string MetaTitle { get; set; }

        public string MetaKeywords { get; set; }
        public string MetaDesc { get; set; }

        public DateTime DatePublic { get; set; }

        public WebNew()
        {

        }

        public WebNew(int id, string title, string image_intro, int hot_news, string alias, int status, string short_description, string content, int category_id, string tags,
            string meta_title, string meta_keywords, string meta_desc, DateTime dateend)
        {
            this.Alias = alias;
            this.CategoryId = (int)category_id;
            this.Content = content;
            this.DatePublic = (DateTime)dateend;
            this.HotNew = (int)hot_news;
            this.ImageIntro = image_intro;
            this.MetaKeywords = meta_keywords;
            this.MetaTitle = meta_title;
            this.ShortDes = short_description;
            this.Status = (int)status;
            this.Tags = tags;
            this.Title = title;
            this.MetaDesc = meta_desc;
        }

    }
}