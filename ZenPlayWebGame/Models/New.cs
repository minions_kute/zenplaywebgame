﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZenPlayWebGame.Models
{
    public class New
    {
        public int Id { get; set; }
        public int HotNew { get; set; }//0, 1: small, 2: big
        public string Href { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }
        public DateTime MDateTime { get; set; }
        public string Tags { get; set; }
    }
}