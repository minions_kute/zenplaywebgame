namespace AdminCMS.Models
{
    using System;
    using System.Data.Entity;
    using ZenPlayWebGame.Models;

    public partial class ZenPlayContextWeb : DbContext
    {
        private readonly log4net.ILog log;

        public ZenPlayContextWeb() : base("name=ZenPlayContext")
        {
            log = log == null ? log4net.LogManager.GetLogger(GetType()) : log;
            Database.Log = (dbLog =>
            {
                log.Debug(dbLog);
                Console.WriteLine(dbLog);
                System.Diagnostics.Trace.Write(dbLog);
            });
        }

        public virtual DbSet<WebNew> WebNews { get; set; }
     
    }
}
