﻿using System;
using System.Configuration;
using StackExchange.Redis;
using System.Collections.Generic;
using Newtonsoft.Json;
using AdminCMS.Models;
using System.Linq;

namespace AdminCMS.Common
{
    public class RedisDB
    {
        #region Singleton

        /// <summary>
        /// Singleton holder object
        /// </summary>
        private static RedisDB _instance = null;

        /// <summary>
        /// Singleton object
        /// </summary>
        public static RedisDB Instance { get { if (_instance == null) { _instance = new RedisDB(); } return _instance; } }

        /// <summary>
        /// Database number (0-16/default)
        /// </summary>
        public static int DatabaseIndex = 0;

        #endregion

        #region Init class

        /// <summary>
        /// Redis connection string
        /// </summary>
        private string _connectionString { get { return ConfigurationManager.ConnectionStrings["RedisDB"].ConnectionString; } }

        /// <summary>
        /// Redis holder object
        /// </summary>
        public ConnectionMultiplexer redis = null;

        /// <summary>
        /// Redis database holder object
        /// </summary>
        public IDatabase DB = null;

        /// <summary>
        /// Constructor class
        /// </summary>
        public RedisDB() { if (redis == null) redis = ConnectionMultiplexer.Connect(_connectionString); if (DB == null) DB = redis.GetDatabase(DatabaseIndex); }

        #endregion

        #region Event via pub/sub of redis

        /// <summary>
        /// Key nhận event cho bot tạo bàn chơi
        /// </summary>
        public const string EVENT_GAME_SERVER_INIT_BOT = "event_game_server_init_bot";

        /// <summary>
        /// Sub to channel
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="subChannel"></param>
        /// <param name="channel"></param>
        public static void Subscription(Action<RedisChannel, RedisValue> handler, string subChannel = "all", string channel = "global")
        {
            if (handler != null)
            {
                string channelKey = String.Format("{0}:{1}", channel, subChannel);
                System.Diagnostics.Debug.WriteLine("Started Subscription To Channel: '{0}'", channelKey);

                Instance.redis.GetSubscriber().Unsubscribe(channelKey, handler);
                Instance.redis.GetSubscriber().Subscribe(channelKey, handler);
            }
        }

        /// <summary>
        /// Un-Sub from channel
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="subChannel"></param>
        /// <param name="channel"></param>
        public static void UnSubscription(Action<RedisChannel, RedisValue> handler, string subChannel = "all", string channel = "global")
        {
            string channelKey = String.Format("{0}:{1}", channel, subChannel);
            System.Diagnostics.Debug.WriteLine("Un-Subscription To Channel: '{0}'", channelKey);

            Instance.redis.GetSubscriber().Unsubscribe(channelKey, handler);
        }

        /// <summary>
        /// Un-Sub all channel
        /// </summary>
        public static void UnSubscription()
        {
            System.Diagnostics.Debug.WriteLine("Un-Subscription To All Channel");
            Instance.redis.GetSubscriber().UnsubscribeAll();
        }

        /// <summary>
        /// Pub message to channel
        /// </summary>
        /// <param name="message"></param>
        /// <param name="subChannel"></param>
        /// <param name="channel"></param>
        /// <returns></returns>
        public static long Publish(string message, string subChannel = "all", string channel = "global")
        {
            if (Instance.redis.GetSubscriber() == null) return 0;
            string channelKey = String.Format("{0}:{1}", channel, subChannel);
            System.Diagnostics.Debug.WriteLine("Publish To Channel: '{0}'\nMessage: {1}", channel, message);
            return Instance.redis.GetSubscriber().Publish(channelKey, message);
        }

        /// <summary>
        /// History list key
        /// </summary>
        private static string _chatHistory = "ChatHistory";
        private static int _chatHistoryLength = 50;

        /// <summary>
        /// Store message in history list
        /// </summary>
        /// <param name="message"></param>
        public static void SaveChatMessage(string message)
        {
            Instance.DB.ListTrim(_chatHistory, 0, _chatHistoryLength - 1);
            Instance.DB.ListLeftPush(_chatHistory, message);
        }

        /// <summary>
        /// Get chat history
        /// </summary>
        /// <returns></returns>
        public static List<string> GetChatMessage()
        {
            return new List<string>(Array.ConvertAll(Instance.DB.ListRange(_chatHistory, 0, -1), x => (string)x));
        }

        public enum OS { WINDOW, WEB, ANDROID, IOS }

        /// <summary>
        /// Get version game
        /// </summary>
        /// <returns></returns>
        public static string GetVersion(OS os)
        {
            var version = Instance.DB.StringGet(string.Format("version:{0}", os));
            if (String.IsNullOrEmpty(version)) return "1";
            return version;
        }

        /// <summary>
        /// Set version game
        /// </summary>
        /// <returns></returns>
        public static void SetVersion(string version, OS os)
        {
            Instance.DB.StringSet(string.Format("version:{0}", os), version);
        }

        /// <summary>
        /// Get maintain flag
        /// </summary>
        /// <returns></returns>
        public static bool GetMaintain()
        {
            var maintain = Instance.DB.StringGet("maintain");
            if (String.IsNullOrEmpty(maintain)) return false;
            return maintain == "1" ? true : false;
        }

        /// <summary>
        /// Set maintain flag
        /// </summary>
        /// <returns></returns>
        public static void SetMaintain(bool isMaintain)
        {
            Instance.DB.StringSet("maintain", isMaintain ? "1" : "0");
        }

        /// <summary>
        /// Get maintain message
        /// </summary>
        /// <returns></returns>
        public static string GetMaintainMessage()
        {
            string maintain = Instance.DB.StringGet("maintain_message");
            if (String.IsNullOrEmpty(maintain)) return "";
            return maintain;
        }

        /// <summary>
        /// Set maintain message
        /// </summary>
        /// <param name="message"></param>
        public static void SetMaintainMessage(string message)
        {
            Instance.DB.StringSet("maintain_message", message);
        }

        /// <summary>
        /// Get event title
        /// </summary>
        /// <returns></returns>
        public static string GetGiftEventTitle()
        {
            return Instance.DB.StringGet("gift_event_title");
        }

        /// <summary>
        /// Set event title
        /// </summary>
        /// <param name="guide"></param>
        public static void SetGiftEventTitle(string guide)
        {
            Instance.DB.StringSet("gift_event_title", guide);
        }

        /// <summary>
        /// Get event guide
        /// </summary>
        /// <returns></returns>
        public static string GetGiftEventGuide()
        {
            return Instance.DB.StringGet("gift_event_guide");
        }

        /// <summary>
        /// Set event guide
        /// </summary>
        /// <param name="guide"></param>
        public static void SetGiftEventGuide(string guide)
        {
            Instance.DB.StringSet("gift_event_guide", guide);
        }

        #endregion


        /// <summary>
        /// Prefix key for user
        /// </summary>
        private static string _userKey = "User";
        private static string _userNameKey = "User:UserName";
        private static string _userInfoKey = "User:Info";

        /// <summary>
        /// Get User by ID
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static T GetUser<T>(int UserId) where T : class
        {
            string userKey = String.Format("{0}:{1}", _userKey, UserId);
            T User = null;
            try
            {
                RedisValue json = Instance.DB.StringGet(userKey);
                User = JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Get User by id {0} failed: '{1}'", UserId, e.Message);
                User = null;
            }
            return User;
        }

        /// <summary>
        /// Get User by UserName
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public static T GetUser<T>(string UserName) where T : class
        {
            string userKey = String.Format("{0}:{1}", _userNameKey, UserName);
            int UserId = (int)Instance.DB.StringGet(userKey);
            return GetUser<T>(UserId);
        }

        /// <summary>
        /// Get user extra information
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static T GetUserExtraInfo<T>(int UserId) where T : class
        {
            string userKey = String.Format("{0}:{1}", _userInfoKey, UserId);
            T ExtraInfo = null;
            try
            {
                RedisValue json = Instance.DB.StringGet(userKey);
                ExtraInfo = JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Get User by id {0} failed: '{1}'", UserId, e.Message);
                ExtraInfo = null;
            }
            return ExtraInfo;
        }

        /// <summary>
        /// Set User by ID and Data Model
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="UserId"></param>
        /// <param name="User"></param>
        /// <returns></returns>
        public static bool SetUser<T>(int UserId, T User) where T : class
        {
            string userKey = String.Format("{0}:{1}", _userKey, UserId);
            string json = JsonConvert.SerializeObject(User);
            return Instance.DB.StringSet(userKey, json, new TimeSpan(7, 0, 0, 0, 0));
        }

        /// <summary>
        /// Set user extra information
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="UserId"></param>
        /// <param name="ExtraInfo"></param>
        /// <returns></returns>
        public static bool SetUserExtraInfo<T>(int UserId, T ExtraInfo) where T : class
        {
            string userKey = String.Format("{0}:{1}", _userInfoKey, UserId);
            string json = JsonConvert.SerializeObject(ExtraInfo);
            return Instance.DB.StringSet(userKey, json);
        }

        /// <summary>
        /// Set User by ID and UserName
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="UserId"></param>
        /// <param name="UserName"></param>
        /// <param name="User"></param>
        /// <returns></returns>
        public static bool SetUser<T>(int UserId, string UserName, T User) where T : class
        {
            string userKey = String.Format("{0}:{1}", _userNameKey, UserName);
            Instance.DB.StringSet(userKey, UserId);
            return SetUser(UserId, User);
        }

        /// <summary>
        /// Get Users by ID array
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="UserIds"></param>
        /// <returns></returns>
        public static List<T> GetUsers<T>(int[] UserIds) where T : class
        {
            List<T> Users = new List<T>();
            List<RedisKey> userKeys = new List<RedisKey>();

            // build multi keys
            foreach (int UserId in UserIds) { string userKey = String.Format("{0}:{1}", _userKey, UserId); userKeys.Add(userKey); }

            // redis c# quá ngu, thay vì dùng MGET thì dùng multi get...
            RedisValue[] json = Instance.DB.StringGet(userKeys.ToArray());

            // convert to array user object
            foreach (RedisValue data in json)
            {
                try
                {
                    T User = JsonConvert.DeserializeObject<T>(data);
                    Users.Add(User);
                }
                catch { }
            }

            return Users;
        }

        public static string GetLinkAndroid()
        {
            try
            {
                return Instance.DB.StringGet("link_android");
            }
            catch (Exception)
            {
                return "http://157.119.247.10/zenplay.apk";
            }
        }

        public static string GetLinkIOS()
        {
            try
            {
                return Instance.DB.StringGet("link_ios");
            }
            catch (Exception)
            {
                return "http://ios.zenplay68.com/";
            }
            
        }
    }
}