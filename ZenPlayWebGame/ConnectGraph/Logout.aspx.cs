﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ConnectGraph_Logout : System.Web.UI.Page
{
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            // Redirect sang graph authen
            string str = ServerProcess.GetRejectTokenUrl();
            Response.Redirect(str);
        }
        catch (Exception ex)
        {
            log.Error(ex);
        }
    }
}